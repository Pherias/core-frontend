import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import Register from '@/components/Register'
import Users from '@/components/Users'
import User from '@/components/User'
import Home from '@/components/Home'
import Usage from '@/components/Usage';

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard,
            children: [{
                    name: 'Home',
                    path: '/',
                    component: Home
                }, {
                    name: 'Register',
                    path: '/register',
                    component: Register
                },
                {
                    name: 'Users',
                    path: '/users',
                    component: Users
                },
                {
                    name: 'User',
                    path: '/users/:id',
                    component: User
                },
                {
                    name: 'Usage',
                    path: '/usage',
                    component: Usage
                }
            ]
        }
    ],
    mode: 'history'
})
