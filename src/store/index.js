import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: JSON.parse(localStorage.getItem('user')) || null,
        token: localStorage.getItem('token') || ''
    },
    mutations: {
        login(state, val) {
            const { user, token } = val;

            state.user = user;
            state.token = token;

            localStorage.setItem('user', JSON.stringify(user));
            localStorage.setItem('token', token);
        },
        logout(state) {
            state.user = null;
            state.token = '';

            localStorage.removeItem('user');
            localStorage.removeItem('token');
        },
        updateUser(state, val) {
            state.user = val;

            localStorage.setItem('user', JSON.stringify(state.user));
        }
    },
    getters: {
        getUser: state => state.user,
        getToken: state => state.token
    }
})