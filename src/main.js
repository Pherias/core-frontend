// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import store from './store'
import router from './router'

import axios from 'axios'
import VueAxios from 'vue-axios'

import Config from './config';
import Transitions from './modules/transitions'
import Notifications from './modules/notifications'
import PopupManager from './modules/PopupManager'

import Button from './components/other/Button'

Vue.component('x-button', Button);

Vue.use(VueAxios, axios)

Vue.prototype.config = Config;
Vue.prototype.transitions = new Transitions();
Vue.prototype.notifications = new Notifications();
Vue.prototype.popup = new PopupManager();

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})
