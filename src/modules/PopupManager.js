class PopupManager {

    set(vue) {
        this.vue = vue;
    }

    open(info, onSucces, onCancel) {

        this.vue.open(info, onSucces, onCancel);
    }
}


export default PopupManager;