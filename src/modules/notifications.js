export default class Notifications {

    set(component) {
        this.component = component;
    }

    notify(message, duration, type) {

        if (!this.component) {
            console.log('notifications component has not been initialized')
            return;
        }

        const info = {
            message: message || '',
            duration: duration || 2000,
            type: type || 'default'
        }

        this.component.notify(info);
    }
}