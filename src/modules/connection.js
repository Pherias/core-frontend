import { io } from 'socket.io/client-dist/socket.io'

export default class Connection {
    'Websocket connection'

    constructor(token, vue) {
        this.token = token;
        this.socket = io(vue.config.socketEndpoint, {
            query: {
                token: token
            }
        })
    }

    on(event, listener) {
        this.socket.on(event, listener);
    }

    emit(event, data) {
        this.socket.emit(event, data);
    }

    disconnect() {
        this.socket.disconnect();
    }
}