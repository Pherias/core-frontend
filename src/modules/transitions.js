export default class PanelTransition {

    load(vue) {
        if (!vue.$refs.panel || !vue.$refs.loading) return;

        vue.$refs.panel.classList = "panel end"
        vue.$refs.loading.classList = "hidden"

        setTimeout(() => {
            vue.$refs.panel.classList = "hidden"
            vue.$refs.loading.classList = "panel"

        }, 200);
    }

    endLoad(vue) {
        if (!vue.$refs.loading || !vue.$refs.panel) return;

        vue.$refs.loading.classList = "panel end"
        vue.$refs.panel.classList = "hidden"

        setTimeout(() => {
            vue.$refs.loading.classList = "hidden"
            vue.$refs.panel.classList = "panel"

        }, 200);
    }
}
