export default {
    backendEndpoint: process.env.NODE_ENV == 'development' ? 'http://localhost:7000' : 'https://core-authentication-service.herokuapp.com',
    socketEndpoint: process.env.NODE_ENV == 'development' ? 'http://localhost:5000' : 'https://core-socket-service.herokuapp.com',
    notificationService: process.env.NODE_ENV == 'development' ? 'http://localhost:5500' : 'https://core-notification-service.herokuapp.com',
    usageService: process.env.NODE_ENV == 'development' ? 'http://localhost:4001' : 'https://core-usage-service.herokuapp.com',
}
